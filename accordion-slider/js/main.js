// Esperar que el DOM esté listo
document.addEventListener('DOMContentLoaded', function() {
	
	// Variable cache
	var $contenido = $('.panel__contenido'),
		$cabecera = $('.panel__cabecera'),
		$creditos = $('.creditos');
		

	// Mostramos el contenido del primer panel
	$contenido.first().slideDown(500);
	$cabecera.first().addClass('cabecera_activa');
	$('.flip').first().addClass('flip-abierto');


	// Event listeners
	$cabecera.click(function(evt) {
		evt.preventDefault();
		$contenido.not(this).each(function() {
			$(this).slideUp(500);
			$cabecera.removeClass('cabecera_activa');
			$cabecera.children('.flip').removeClass('flip-abierto');
		});
		$(this).siblings('.panel__contenido').slideDown(500);
		$(this).addClass('cabecera_activa');
		$(this).children('.flip').addClass('flip-abierto');
	});


	// Le añadimos animación a los créditos
	$creditos.hover(function(){
		$(this).toggleClass('animated infinite swing');
	}); 

});