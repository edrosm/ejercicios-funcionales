// Esperar que el DOM esté listo
document.addEventListener('DOMContentLoaded', function() {
	
	
	// Capturar '.hover()'
	$('a').hover(function() {
		// hover in:

		// Guardamos el atributo 'title' de cada enlace en una variable
		var $titulo = $(this).attr('title');

		// Guardamos el 'title nativo' en un atributo 'data-' y lo eliminamos 
		$(this).data('titulo', $titulo).removeAttr('title');

		// Añadimos nuestro propio 'tooltip'
		$('<p class="tooltip"></p>')
			.text($titulo)
			.appendTo('body')
			.fadeIn('slow')
			.css('background-color', $(this).data('fondo')); 

	}, function() {
		// hover out:
		
		// Reponemos el 'tittle nativo'
		$(this).attr('title', $(this).data('titulo'));

		// Eliminamos nuestro 'tooltip'
		$('.tooltip').remove();

	}).mousemove(function(e) {
		// Capturar la posición X e Y del ratón sobre el elemento que lanza nuestro 'tooltip'
		var posicionRatonX = e.pageX + 20,
			posicionRatonY = e.pageY + 10;

		// Hacer que el 'tooltip' se mueva junto con el puntero
		$('.tooltip').css({
			'top': posicionRatonY,
			'left': posicionRatonX
		});
	});


	// Añadimos animación a los créditos
	$('.creditos').hover(function(){
		$(this).toggleClass('animated infinite swing');
	}); 


});