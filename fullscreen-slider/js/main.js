// Esperar que el DOM esté listo
document.addEventListener('DOMContentLoaded', function() {
	
	// Buscamos los elementos
	var $flechas = $('body').find('.flechas'),
		$lis = $('body').find('li'),
		$imagenes = $lis.find('img'),
		$creditos = $('.creditos'),
		imgActiva = 0;
		ctdImagenes = $imagenes.length;

	// --------------------------------------------------------------

	// Les añadimos animación a las flechas
	$flechas.hover(function() {
		$(this).toggleClass('animated wobble');
	});

	// -----------------------------------
	// -----------------------------------

	// Le añadimos animación a los créditos
	$creditos.hover(function(){
		$(this).toggleClass('animated tada');
	});

	// -----------------------------------

	// Ocultamos todas las imágenes
	$lis.hide();

	// -----------------------------------

	// Eliminamos las imágenes como elemento/tag y ¬
	// -> las añadimos como fondo de su 'parent' ('li')
	$imagenes.each(function(indice, imagen) {
		$lis.eq(indice).css({
			'background-image': 'url(' + $(imagen).attr('src') + ')',
			'background-size': 'cover',
			'background-repeat': 'no-repeat',
			'background-position': 'center center',
			'position': 'absolute'
		});
		// Una vez hecho esto, eliminamos la imagen
		$(imagen).remove();
	});

	// -----------------------------------

	// Mostramos la primera imagen
	$lis.eq(imgActiva).fadeIn(400);

	// -----------------------------------
	
	// Funcionalidad de las flechas

	// Creamos una función que muestre las imágenes
	function mostrarImagen() {
		$lis.hide();
		$lis.eq(imgActiva).fadeIn(400);
	}

	// Flecha derecha
	$flechas.filter('#flecha-derecha').click(function(evt) {
		evt.preventDefault();
		imgActiva += 1;
		if (imgActiva > ctdImagenes - 1) {
			imgActiva = 0;
		}
		mostrarImagen();
	});

	// Flecha izquierda
	$flechas.filter('#flecha-izquierda').click(function(evt) {
		evt.preventDefault();
		imgActiva -= 1;
		if (imgActiva < 0) {
			imgActiva = ctdImagenes - 1;
		}
		mostrarImagen();
	});

	// -----------------------------------

});